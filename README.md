# jb_bool_attr

An AngularDart 1 Component for being re-used in other AngularDart projects

## Usage
1. Add the component to your pubspec.yaml

    Use git https version (better for public dependencies)  
            
            dependencies:
              jb_bool_attr:
                git: https://<insert URL>
                [ref]: some-branch | tag


2. Show your component to angular dependency injection -> see App Class in app_initialize.dart

        import 'package:jb_bool_attr/jb_bool_attr.dart';

        class App extends Angular.Module {

          App() {
            bind(JbBoolAttr);
          }
        }

## Tag Usage

<any-tag jb-bool-attr-*="true|false">

arbitrary boolean html attribute = *

If true => * will be added  
If false => * will be removed

Usage with interpolation
<any-tag jb-bool-attr-*="{{boolean var or expression}}">