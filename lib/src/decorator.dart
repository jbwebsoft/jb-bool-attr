library jb_bool_attr.component;

import 'package:angular/angular.dart';
import 'package:angular/core_dom/module_internal.dart';

///Two Sources:
///directives for inspiration
///https://github.com/angular/angular.dart/blob/d5f45a695c316df5492d6ad764257e0b9f81377e/lib/directive/ng_src_boolean.dart
///
/// dependencies for that directives:
/// https://github.com/angular/angular.dart/blob/d5f45a695c316df5492d6ad764257e0b9f81377e/lib/directive/module.dart
@Decorator(selector: "[jb-bool-attr-*]")
class JbBoolAttr implements AttachAware {
  static const String ngAttrPrefix = 'jb-bool-attr-';

  final NgElement _node;

  final NodeAttrs _attrs;

  JbBoolAttr(this._node, this._attrs);

  @override
  void attach() {
    _attrs.forEach((key, value) {
      if (key.startsWith(ngAttrPrefix)) {
        var newKey = key.substring(ngAttrPrefix.length);

        _attrs.observe(key, (newValue) => _toggleAttribute(newKey, newValue));

        _toggleAttribute(newKey, value);
      }
    });
  }

  void _toggleAttribute(attrName, on) {
    if (on == "true") {
      _node.setAttribute(attrName);
    } else {
      _node.removeAttribute(attrName);
    }
  }
}
