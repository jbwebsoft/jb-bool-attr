library jb_bool_attr.app_init;

import 'package:angular/application_factory.dart';
import 'package:angular/angular.dart' as Angular;
import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';

//import own component
import 'package:jb_bool_attr/jb_bool_attr.dart';

final Logger _libLogger = new Logger("JbBoolAttr");

void main() {
  //init logging
  hierarchicalLoggingEnabled = true;
  Logger.root.onRecord.listen(new LogPrintHandler());

  Logger.root.level = Level.OFF;
  _libLogger.level = Level.ALL;

  applicationFactory().addModule(new App()).rootContextType(TempRootScope).run();
}

class App extends Angular.Module {

  App() {
    bind(JbBoolAttr);
  }

}

@Angular.Injectable()
class TempRootScope {
  String content = "Example Content";

  bool inputDisabled = false;

  void toggleDisabled() {
    inputDisabled = !inputDisabled;
  }

}